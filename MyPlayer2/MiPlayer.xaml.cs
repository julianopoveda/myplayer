﻿using Microsoft.Win32;
using System;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Threading;

namespace MyPlayer2
{
    /// <summary>
    /// Interaction logic for MiPlayer.xaml
    /// </summary>
    public partial class MiPlayer : UserControl
    {
        #region DependencyProperties
        private static DependencyProperty ShowPlayPauseButtonProperty = DependencyProperty.Register("ShowPlayPauseButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowStopButtonProperty = DependencyProperty.Register("ShowStopButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowNextButtonProperty = DependencyProperty.Register("ShowNextButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowPrevButtonProperty = DependencyProperty.Register("ShowPrevButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowVolumeButtonProperty = DependencyProperty.Register("ShowVolumeButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowFixUnfixButtonProperty = DependencyProperty.Register("ShowFixUnfixButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty ShowOpenButtonProperty = DependencyProperty.Register("ShowOpenButton", typeof(Visibility), typeof(MiPlayer));
        private static DependencyProperty LoadedBehaviorProperty = DependencyProperty.Register("LoadedBehavior", typeof(MediaState), typeof(MiPlayer));
        private static DependencyProperty UnloadedBehaviorProperty = DependencyProperty.Register("UnloadedBehavior", typeof(MediaState), typeof(MiPlayer));
        private static DependencyProperty SourceProperty = DependencyProperty.Register("Source", typeof(Uri), typeof(MiPlayer));
        private static DependencyProperty IsStatusBarVisibleProperty = DependencyProperty.Register("IsStatusBarVisible", typeof(bool), typeof(MiPlayer));
        #endregion
        #region Propriedades
        private MediaElement _player { get; set; }
        private DispatcherTimer _time;

        public Visibility ShowPlayPauseButton
        {
            get { return (Visibility)GetValue(ShowPlayPauseButtonProperty); }
            set { SetValue(ShowPlayPauseButtonProperty, value); }
        }

        public Visibility ShowStopButton
        {
            get { return (Visibility)GetValue(ShowStopButtonProperty); }
            set { SetValue(ShowStopButtonProperty, value); }
        }

        public Visibility ShowNextButton
        {
            get { return (Visibility)GetValue(ShowNextButtonProperty); }
            set { SetValue(ShowNextButtonProperty, value); }
        }

        public Visibility ShowPrevButton
        {
            get { return (Visibility)GetValue(ShowPrevButtonProperty); }
            set { SetValue(ShowPrevButtonProperty, value); }
        }

        public Visibility ShowFixUnfixButton
        {
            get { return (Visibility)GetValue(ShowFixUnfixButtonProperty); }
            set { SetValue(ShowFixUnfixButtonProperty, value); }
        }

        public Visibility ShowVolumeButton
        {
            get { return (Visibility)GetValue(ShowVolumeButtonProperty); }
            set { SetValue(ShowVolumeButtonProperty, value); }
        }

        public Visibility ShowOpenButton
        {
            get { return (Visibility)GetValue(ShowOpenButtonProperty); }
            set { SetValue(ShowOpenButtonProperty, value); }
        }

        public MediaState LoadedBehavior
        {
            get { return (MediaState)GetValue(LoadedBehaviorProperty); }
            set { SetValue(LoadedBehaviorProperty, value); }
        }

        public MediaState UnloadedBehavior
        {
            get { return (MediaState)GetValue(UnloadedBehaviorProperty); }
            set { SetValue(UnloadedBehaviorProperty, value); }
        }

        /// <summary>
        /// Valor inicial do volume entre 0 e 100
        /// </summary>
        public double InitialVolume { get; set; }

        public bool IsStatusBarVisible
        {
            get { return (bool)GetValue(IsStatusBarVisibleProperty); }
            set { SetValue(IsStatusBarVisibleProperty, value); }
        }

        public Uri Source
        {
            get { return (Uri)GetValue(SourceProperty); }
            set { SetValue(SourceProperty, value); }
        }
        #endregion

        public MiPlayer()
        {
            InitializeComponent();
        }

        #region Internal Suport Functions
        /// <summary>
        /// Inicializa as opções definidas pelo usuário
        /// </summary>
        private void InitOptions()
        {
            _player = mediaplayer;
            _player.LoadedBehavior = LoadedBehavior;
            _player.UnloadedBehavior = UnloadedBehavior;
            _player.Source = this.Source;
            btnPlayPauseVideo.Visibility = ShowPlayPauseButton;
            btnStopVideo.Visibility = ShowStopButton;
            btnNextVideo.Visibility = ShowNextButton;
            btnPrevVideo.Visibility = ShowPrevButton;
            btnFixUnfix.Visibility = ShowFixUnfixButton;
            btnOpenVideo.Visibility = ShowOpenButton;
            btnVolume.Visibility = ShowVolumeButton;

            //Volume options
            sliderVolume.Value = InitialVolume / 100;
            sliderVolume.Minimum = 0;
            sliderVolume.Maximum = 1;
            sliderVolume.Visibility = Visibility.Hidden;

            //Inicializa a função por aqui. Caso contrário ocorre erro
            sliderVolume.ValueChanged += sliderVolume_ValueChanged;
            if (IsStatusBarVisible)
                btnFixUnfix_Click(btnFixUnfix, new RoutedEventArgs());
        }

        /// <summary>
        /// Inicializa a contabilização do tempo pelo slider referente ao audio ou vídeo que esta sendo reproduzido
        /// </summary>
        private void InitTimer()
        {
            try
            {
                _time = new DispatcherTimer();//Instancia o objeto
                _time.Tick += ChangeTimeValue;//Define a função a ser executada
                _time.Interval = TimeSpan.FromSeconds(1);//inicializa o intervalo
                _time.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private MediaState GetMediaState(MediaElement myMedia)
        {
            FieldInfo hlp = typeof(MediaElement).GetField("_helper", BindingFlags.NonPublic | BindingFlags.Instance);
            object helperObject = hlp.GetValue(myMedia);
            FieldInfo stateField = helperObject.GetType().GetField("_currentState", BindingFlags.NonPublic | BindingFlags.Instance);
            MediaState state = (MediaState)stateField.GetValue(helperObject);
            return state;
        }

        /// <summary>
        /// Convert to string and format this string as hh:mm:ss a timespan
        /// </summary>
        /// <param name="totaltime">the timespan to converting</param>
        /// <returns>time formated as hh:mm:ss</returns>
        private string GetFormatedTime(TimeSpan totaltime)
        {
            TimeSpan returnTime = new TimeSpan(totaltime.Hours, totaltime.Minutes, totaltime.Seconds);
            return returnTime.ToString(@"hh\:mm\:ss");
        }

        /// <summary>
        /// Atualizar a posição do slider e o label de progresso
        /// </summary>
        private void ChangeTimeValue(object sender, EventArgs e)
        {
            sliderTime.Value = mediaplayer.Position.TotalSeconds;
            lblProgressTime.Text = GetFormatedTime(mediaplayer.Position);

        }
        #endregion

        #region Button Click Actions
        private void btnPlayPauseVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_player.Source == null)
                    return;

                if (GetMediaState(_player) != MediaState.Play)
                {
                    _player.Play();
                    btnPlayPauseVideo.Content = "\uE769";
                    InitTimer();
                }
                else
                {
                    _player.Pause();
                    btnPlayPauseVideo.Content = "\uE768";
                    if (_time != null && _time.IsEnabled)
                        _time.Stop();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnStopVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _player.Close();
                btnPlayPauseVideo.Content = "\uE768";//Garante o botão de play de novo

                if (_time != null && _time.IsEnabled)
                    _time.Stop();

                //Resetar os labels
                lblTotalTime.Text = "00:00:00";
                lblProgressTime.Text = "00:00:00";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnOpenVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OpenFileDialog objFileDialog = new OpenFileDialog();
                if (objFileDialog.ShowDialog() == true)
                {
                    _player.Source = new Uri(objFileDialog.FileName);
                    _player.Play();
                    _player.Volume = sliderVolume.Value;
                    btnPlayPauseVideo.Content = "\uE769";
                    InitTimer();
                    //Define o valor máximo do slider de acordo com o tamanho do audio/vídeo
                    sliderTime.Maximum = _player.NaturalDuration.TimeSpan.TotalSeconds;

                    //Seta o valor do label total
                    lblTotalTime.Text = GetFormatedTime(_player.NaturalDuration.TimeSpan);

                    //Resetar o label de progress
                    lblProgressTime.Text = "00:00:00";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnVolume_Click(object sender, RoutedEventArgs e)
        {
            Button btnVolume = (Button)sender;
            string volumeOnChar = "\uE767";
            string muteChar = "\uE74F";
            if ((string)btnVolume.Content == volumeOnChar)
            {
                _player.Volume = 0;
                btnVolume.Content = muteChar;
            }
            else
            {
                _player.Volume = sliderVolume.Value;
                btnVolume.Content = volumeOnChar;
            }
        }

        //Fixar e desafixar a barra de tarefas
        private void btnFixUnfix_Click(object sender, RoutedEventArgs e)
        {
            Button btnPin = (Button)sender;
            string pinchar = "\uE718";
            string unpinchar = "\uE77A";

            if ((string)btnPin.Content == pinchar)
            {
                //Desabilita as funções
                playerBar.MouseEnter -= playerBar_MouseEnter;
                playerBar.MouseLeave -= playerBar_MouseLeave;

                //Define a opacidade para 100%(ou 1)
                playerBar.Opacity = 1;

                //Redimensiona o player
                mediaplayer.Height -= mediaplayer.ActualHeight - playerBar.Height;

                //Muda o content para o UnPin
                btnPin.Content = unpinchar;
            }
            else
            {
                //Desfaz o que foi feito no if
                //Desabilita as funções
                playerBar.MouseEnter += playerBar_MouseEnter;
                playerBar.MouseLeave += playerBar_MouseLeave;

                //Define a opacidade para 100%(ou 1)
                playerBar.Opacity = 1;

                //Redimensiona o player
                mediaplayer.Height += mediaplayer.ActualHeight - playerBar.Height;

                //Muda o content para o Pin
                btnPin.Content = pinchar;
            }
        }
        #endregion

        #region Mouse Events
        //Mostra a barra do player
        private void playerBar_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Grid objPlayerBar = (Grid)sender;
            objPlayerBar.Opacity = 1;
            objPlayerBar.MouseEnter -= playerBar_MouseEnter;
        }

        //Oculta a barra do player
        private void playerBar_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Grid objPlayerBar = (Grid)sender;
            objPlayerBar.Opacity = 0;
            //Reabilita a função do mouse enter
            objPlayerBar.MouseEnter += playerBar_MouseEnter;
        }

        private void btnVolume_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            sliderVolume.Visibility = Visibility.Visible;
        }

        private void sliderVolume_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            sliderVolume.Visibility = Visibility.Hidden;
        }
        #endregion

        #region Slider ValueChange Events
        private void sliProgress_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            _player.Position = TimeSpan.FromSeconds(sliderTime.Value);
        }


        private void sliderVolume_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            string volumeOnChar = "\uE767";
            string muteChar = "\uE74F";
            Slider objVolumeControl = (Slider)sender;
            _player.Volume = objVolumeControl.Value;
            //Define o icone do botão de volume para mudo
            if (_player.Volume > 0)
                btnVolume.Content = volumeOnChar;
            else
                btnVolume.Content = muteChar;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitOptions();
        }
        #endregion

        //TODO: Funções para os botões prev e next em listas
        //TODO: Lista de reprodução
    }
}
